package suvey;

import utils.FileUtils;
import utils.Input;
import utils.Output;
import utils.SerializationHelper;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


public class DateQuestion extends EssayQuestion {

    private Integer numResponses;

    /**
     * Displays formatted question to standard out
     * */
    @Override
    public void display() {
        Output.println(this.toString());
    }


    /**
     * Run the process of setting up question object with through standard in
     * */
    @Override
    public void build() {
        int MAX_DATES = 50;

        Output.println("Enter the prompt for your Date question:");
        this.setPrompt(Input.readLine());
        Output.println("Enter the number of choices for your date question.");

        this.numResponses = Input.readIntInRange(1, MAX_DATES);

    }


    /**
     * Run the process of asking through std out and getting
     * a user response from std in
     *
     * @return Question response formatted into a single string
     * */
    @Override
    public Answer ask() {
        StringBuilder responses = new StringBuilder();
        String response;
        Answer answer;
        int i;

        this.display();

        //run for the allowed amount of responses
        for (i=0;i<this.numResponses;i++) {

            //get user input until valid input is received
            do {
                response = Input.readLine();

            } while (!validateResponse(response));

            //parse to single string
            responses.append(response);
            if (i<this.numResponses-1){
                responses.append("\n");
            }

        }
        answer = new Answer(responses.toString());
        return answer;
    }


    /**
     * Run the process of modifying the current question through std io
     * */
    @Override
    public void modify(){
        this.modifyPrompt();
    }

    @Override
    public void displayCorrectAnswer(){

        if (this.correctAnswer==null){
            Output.println("No correct choice set\n");
            return;
        }

        Output.println("The correct date is " + this.correctAnswer.getAnswer() + "\n");
    }

    /**
     * Convert question to formatted string
     *
     * @return formatted question string
     * */
    @Override
    public String toString(){

        return this.getPrompt() +
                "(Expected responses: " + this.numResponses +
                " format: mm-dd-yyyy):" + "\n";
    }


    /**
     * Check to see if response is valid
     *
     * @return indicates if question response string is valid
     * */
    protected boolean validateResponse(String response){

        SimpleDateFormat parser= new SimpleDateFormat("MM-dd-yyyy");
        parser.setLenient(false);

        try {
            //check for valid date
            Date date = parser.parse(response);
        } catch (ParseException e) {
            Output.println("Invalid Date");
            return false;
        }

        return true;
    }


    /**
     * Validate correct answer is possible choice
     *
     * @return indicates if a correct answer is valid
     * */
    @Override
    protected boolean validateCorrectAnswer(Answer answer) {

        if (answer == null){return false;}

        String responseStr = answer.getAnswer();
        List<String> dates = new ArrayList<>(Arrays.asList(responseStr.split("\n")));

        if (dates.size() != numResponses){
            return false;
        }

        for (String date : dates) {
            if (!validateResponse(date)){
                return false;
            }
        }

        return true;
    }

    /**
     * Tabulate all the date questions
     * */
    public void tabulateAnswers(){

        Set<String> choices = new HashSet<String>();
        Map<String, Integer> tab = new HashMap<>();
        Output.println("Question:");
        Output.println(getPrompt());
        Output.println("\nResponses:");
        Output.outputResponses(this.responses);

        // create choices list
        for(Answer response : this.responses){
            choices.add(response.getAnswer());
        }

        // init tabulation values to 0
        for(String choice : choices){
            tab.put(choice, 0);
        }

        // tabulate
        for(Answer response : this.responses){
            tab.put(response.getAnswer(), tab.get(response.getAnswer())+1);
        }

        // output tabulation
        Output.println("\nTabulation:");
        this.display();
        tab.forEach((k,v) -> Output.println(k + "\n" + v + "\n"));
        Output.println("");

    }


    /***** Methods for serialization and deserialization ************************************/

    /**
     * These methods are based on example of serialization from student repo VehicleCityVersion project
     * */

    /**
     * Saves a suvey.Question and all associated attributes in the proper location using the
     * built-in Serialization API
     * @return The path to the file the suvey.Question is saved in
     */
    public String serialize(String name){

        return SerializationHelper.serialize(DateQuestion.class, this, Question.basePath, name);
    }

    /**
     * Deserializes a specific suvey.DateQuestion. The user will be presented with available DateQuestions to
     * deserialize.
     * @return The deserialized suvey.DateQuestion
     */
    public static DateQuestion deserialize() {

        // Use the existing utility function to allow the user to pick the suvey.DateQuestion from a
        // list of existing serialized DateQuestions
        String selectedQuestion = FileUtils.listAndPickFileFromDir(Question.basePath);

        // Use the existing deserialization function to handle it from here
        return deserialize(selectedQuestion);
    }

    /**
     * Deserializes an suvey.DateQuestion that can be found at the given path
     * @param path The path to the car
     * @return The deserialized car
     */
    public static DateQuestion deserialize(String path){

        String fullPath = Question.basePath + path + File.separator;

        return SerializationHelper.deserialize(DateQuestion.class, fullPath);
    }


}
