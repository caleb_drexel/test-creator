package suvey;

import utils.*;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Survey implements Serializable {


    private static final long serialVersionUID = 1L;
    public static final String SERIAL_BASE_PATH = FSConfig.serialDir + "Survey" + File.separator;
    protected final List<Question> questions = new ArrayList<>();
    protected String name;


    public Survey(){

    }

    /*************** Getters and Setters *****************************************************/
    public void addQuestion(Question question){
        this.questions.add(question);
    }

    public List<Question> getQuestions(){return this.questions;}

    public String getName(){return this.name;}

    /*****************************************************************************************/


    /**
     * Get survey name
     * */
    public void generateName(){
        String name;

        do{
            Output.println("Enter survey name:");
            name = Input.readLine();
        }while(!this.validateName(name));

        this.name = name;
    }


    /**
     * Validate correct answer is possible choice
     *
     * @param name survey name
     * @return indicates if a correct answer is valid
     * */
    protected boolean validateName(String name){

        int verify;
        List<String> surveyNames;

        // get name
        try{
            surveyNames = FileUtils.getAllFileNamesInDir(Survey.SERIAL_BASE_PATH);
        } catch (IllegalStateException e){
            surveyNames = new ArrayList<>();
        }

        // check to see if name is already in use
        if (surveyNames.contains(name)){
            Output.println("A survey with this name already exists!");
            Output.println("Do you want to proceed even though the file could be overwritten?");
            Output.println("\t0) no");
            Output.println("\t1) yes");
            verify = Input.readIntInRange(0,1);

            return verify != 0;
        }


        return FileUtils.validateFileName(name);
    }


    /**
     * modify question in survey
     * */
    public void modifyQuestion (){
        int questionIdx;
        Output.println("Which question would you like to modify?");
        questionIdx = Input.readIntInRange(1, this.questions.size())-1;
        this.questions.get(questionIdx).modify();
    }


    public void save(){
        this.serialize(this.name);
    }

    public static Survey load(String name){
        Output.println("Loading Survey!");
        return deserialize(name);
    }


    /**
     * Display all questions in survey
     * */
    public void display(){
        int i;
        String divider = "____________________________________________________________\n";

        Output.println(divider);

        for (i=0; i<this.questions.size();i++){
            System.out.print((i + 1) + ") ");
            this.questions.get(i).display();
            Output.println("");
        }

        Output.println(divider);

    }


    /**
     * Take a survey and save the response
     * */
    public void take(){
        SurveyResponse surveyResponse;
        List<Answer> responses = new ArrayList<>();
        Answer answer;

        // take each question of the survey
        int i;
        for (i=0; i<this.questions.size();i++){
            System.out.print((i+1) + ") ");
            answer = this.questions.get(i).ask();
            this.questions.get(i).addResponse(answer);
            responses.add(answer);
            Output.println("");
        }

        // save the response
        surveyResponse = new SurveyResponse(this, responses);

        surveyResponse.saveStringFile();
        surveyResponse.save();
        save();

    }

    public void tabulate() {

        for (Question question : this.questions){
            question.tabulateAnswers();
        }
    }



    /***** Methods for serialization and deserialization ************************************/

    /**
     * These methods are based on example of serialization from student repo VehicleCityVersion project
     * */

    /**
     * Saves a survey.Survey and all associated attributes in the proper location using the
     * built-in Serialization API
     * @return The path to the file the survey.Survey is saved in
     */
    public String serialize(String name){

        return SerializationHelper.serialize(Survey.class, this, SERIAL_BASE_PATH, name);
    }

    /**
     * Deserializes a specific survey.Survey. The user will be presented with available Surveys to
     * deserialize.
     * @return The deserialized survey.Survey
     */
    public static Survey deserialize() {

        // Use the existing utility function to allow the user to pick the survey.Survey from a
        // list of existing serialized Surveys
        String selectedSurvey = FileUtils.listAndPickFileFromDir(SERIAL_BASE_PATH);

        // Use the existing deserialization function to handle it from here
        return SerializationHelper.deserialize(Survey.class,  selectedSurvey);
    }

    /**
     * Deserializes a survey.Survey that can be found at the given path
     * @param path The path to the car
     * @return The deserialized car
     */
    public static Survey deserialize(String path){


        return SerializationHelper.deserialize(Survey.class,  path);
    }


}
