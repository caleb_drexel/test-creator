package suvey;

import utils.*;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;

public abstract class Question implements Serializable {


    protected static final long serialVersionUID = 1L;
    protected static final String basePath = FSConfig.serialDir + "Question" + File.separator;
    protected String prompt;
    protected ArrayList<Answer> responses = new ArrayList<>();
    protected Answer correctAnswer;

    /*************** Getters and Setters *****************************************************/

    public void setPrompt(String prompt){ this.prompt = prompt; }
    public String getPrompt(){ return this.prompt; }
    public ArrayList<Answer> getResponses(){return this.responses;}
    public void addResponse(Answer response){this.responses.add(response);}

    /****************************************************************************************/

    //abstract methods
    public abstract void display();
    public abstract void build();
    public abstract Answer ask();
    public abstract void modify();
    public abstract void tabulateAnswers();
    protected abstract boolean validateCorrectAnswer(Answer response);
    public abstract void displayCorrectAnswer();


    public Answer getCorrectAnswer(){return this.correctAnswer;}

    public void setCorrectAnswer() {
        Answer correctAnswer;
        if (getClass().equals(EssayQuestion.class)){
            this.correctAnswer = null;
            return;
        }
        Output.println("Enter correct choice");
        correctAnswer = this.ask();
        this.correctAnswer = correctAnswer;
    }

    public void displayPrompt(){
        Output.println(this.prompt + ":");
    }

    /**
     * Facilitates the modification of a question prompt
     * */
    public void modifyPrompt(){
        boolean response;

        this.displayPrompt();
        Output.println("Do you wish to modify the prompt? (yes or no)");
        response = Input.yesNoToTrueFalse();

        //return if no
        if (!response){
            return;
        }

        //get and set new prompt from user
        this.displayPrompt();
        Output.println("Enter a new prompt:");
        this.prompt = Input.readLine();

    }


    /**
     * This function was found on stack overflow. Posted by community wiki Quantum7:
     * https://stackoverflow.com/questions/10813154/how-do-i-convert-a-number-to-a-letter-in-java
     *
     * Turns an int to an alphabetic string where letters are A-Z (base 26)
     *
     * @param i Integer to be converted to base26
     * @return String of alphabetic characters
     * */
    public static String toAlphabetic(int i) {

        if( i<0 ) {
            return "-"+toAlphabetic(-i-1);
        }

        int quot = i/26;
        int rem = i%26;
        char letter = (char)((int)'A' + rem);
        if( quot == 0 ) {
            return ""+letter;
        } else {
            return toAlphabetic(quot-1) + letter;
        }
    }


    /**
     * This function turns an alphabetic string (base26) to an int
     *
     *  @param alphabetic String of alphabetic characters
     *  @return The integer equivalent of the base26 string
     *
     * */
    public static Integer alphabeticToInt(String alphabetic){

        String revAlpha =  new StringBuilder(alphabetic).reverse().toString();
        int strLen = alphabetic.length();
        int i;
        int total = 0;

        for (i=0; i<strLen; i++){
            total+= Math.pow(26, i) * ((int)revAlpha.charAt(i)  - (int)'A'+1);
        }

        return total-1;
    }


    /***** Methods for serialization and deserialization ************************************/

    /**
    * These methods are based on example of serialization from student repo VehicleCityVersion project
    * */

    /**
     * Saves a suvey.Question and all associated attributes in the proper location using the
     * built-in Serialization API
     * @return The path to the file the suvey.Question is saved in
     */
    public String serialize(String name){

        return SerializationHelper.serialize(Question.class, this, basePath, name);
    }

    /**
     * Deserializes a specific suvey.Question. The user will be presented with available Questions to
     * deserialize.
     * @return The deserialized suvey.Question
     */
    public static Question deserialize() {

        // Use the existing utility function to allow the user to pick the suvey.Question from a
        // list of existing serialized Questions
        String selectedQuestion = FileUtils.listAndPickFileFromDir(basePath);

        // Use the existing deserialization function to handle it from here
        return SerializationHelper.deserialize(Question.class,  selectedQuestion);
    }

    /**
     * Deserializes a suvey.Question that can be found at the given path
     * @param path The path to the car
     * @return The deserialized car
     */
    public static Question deserialize(String path){

        String fullPath = basePath + path + File.separator;

        return SerializationHelper.deserialize(Question.class,  fullPath);
    }


}
