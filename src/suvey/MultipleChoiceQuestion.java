package suvey;

import utils.FileUtils;
import utils.Input;
import utils.Output;
import utils.SerializationHelper;

import java.io.File;
import java.util.*;


public class MultipleChoiceQuestion extends Question {

    private Integer numChoices;
    private Integer numResponses;
    protected List<String> choices = new ArrayList<>();


    /**
     * Displays formatted question to standard out
     * */
    @Override
    public void display(){
        Output.println(this.toString());
    }


    /**
     * Run the process of setting up question object with through standard in
     * */
    @Override
    public void build() {

        int i;
        int MIN_CHOICES = 2;
        int MAX_CHOICES = 50;

        //Setup prompt
        Output.println("Enter the prompt for your multiple-choice question:");
        this.setPrompt(Input.readLine());

        //Get number of multiple-choice questions
        Output.println("Enter the number of choices for your multiple-choice question.");
        this.numChoices = Input.readIntInRange(MIN_CHOICES, MAX_CHOICES);

        //get expected amount of responses
        Output.println("Enter the number of expected responses for your multiple-choice question.");
        this.numResponses = Input.readIntInRange(1,numChoices);

        //populate choices
        for (i=1;i<this.numChoices+1; i++){
            Output.println(getPrompt() + ", Enter choice #" + i);
            choices.add(Input.readLine());
        }
    }


    /**
     * Run the process of asking through std out and getting
     * a user response from std in
     *
     * @return Question response formatted into a single string
     * */
    @Override
    public Answer ask() {

        StringBuilder responses = new StringBuilder();
        String response;
        Answer answer;
        int i;
        this.display();

        //run for the allowed amount of responses
        for (i=0;i<this.numResponses;i++) {

            //get user input until valid input is received
            do {
                response = Input.readLine().toUpperCase(Locale.ROOT);
            } while (!this.validateResponse(response));

            //parse to single string
            responses.append(response);
            if (i<this.numResponses-1){
                responses.append("\n");
            }
        }
        answer = new Answer(responses.toString());
        return answer;
    }

    @Override
    public void displayCorrectAnswer(){

        if (this.correctAnswer==null){
            Output.println("No correct choice set\n");
            return;
        }

        Output.println("The correct choice(s):\n" + this.correctAnswer.getAnswer() + "\n");
    }

    /**
     * Run the process of modifying the current question through std io
     * */
    @Override
    public void modify() {

        String response;
        int choiceIndex;

        this.modifyPrompt();

        Output.println("Do you want to modify choices? (yes or no)");

        if (!Input.yesNoToTrueFalse()) {
            return;
        }

        //allow for choices to be updated indefinitely
        while (true) {
            Output.println("Which choice do you want to modify?:");
            this.displayChoices();

            //get user input until valid input is received
            do {
                response = Input.readLine().toUpperCase(Locale.ROOT);
            } while (!this.validateResponse(response));

            Output.println("Choice is entered. Enter new value:");

            choiceIndex = alphabeticToInt(response);

            this.choices.set(choiceIndex, Input.readLine());

            Output.println("Choice modified!");
            Output.println("Would you like to modify another choice? (yes or no)");

            if (!Input.yesNoToTrueFalse()){
                return;
            }

        }
    }


    /**
     * Convert question to formatted string
     *
     * @return formatted question string
     * */
    @Override
    public String toString(){

        return this.getPrompt() +
                " ("+ this.numResponses +
                " response(s) expected)" + "\n" +
                this.generateChoicesStr() + "\n";

    }


    public void displayChoices() {

        Output.println(generateChoicesStr());
    }

    /**
     * Format choices into a string
     *
     * @return formatted choices string
     * */
    public String generateChoicesStr (){

        StringBuilder output = new StringBuilder();
        int i;

        for(i=0;i<choices.size();i++){

            output.append("\t").append(toAlphabetic(i)).append(") ").append(choices.get(i));

            if (i<choices.size()-1){
                output.append("\n");
            }

        }

        return output.toString();
    }


    /**
     * Convert question to formatted string
     *
     * @return indicates if question response string is valid
     * */
    public boolean validateResponse(String response){

        int strLen = response.length();
        String errorMessage = "Invalid Input";
        int numVal;
        int i;
        char c;

        //check to see all chars are alphabetic
        for (i=0; i<strLen; i++){
            c = response.charAt(i);
            if (c < 'A' || c > 'Z'){
                Output.println(errorMessage);
                return false;
            }
        }

        numVal = alphabeticToInt(response);
        if (numVal > this.numChoices-1){
            Output.println(errorMessage);
            return false;
        }

        return true;

    }


    /**
     * Validate correct answer is possible choice
     *
     * @return indicates if a correct answer is valid
     * */
    @Override
    protected boolean validateCorrectAnswer(Answer answer) {

        if (answer == null){return false;}

        String responseStr = answer.getAnswer();
        List<String> dates = new ArrayList<>(Arrays.asList(responseStr.split("\n")));

        if (dates.size() != numResponses){
            return false;
        }

        for (String date : dates) {
            if (!validateResponse(date)){
                return false;
            }
        }

        return true;
    }


    /**
     * Tabulate all the date questions
     * */
    public void tabulateAnswers(){

        // output responses
        Map<String, Integer> tab = new HashMap<>();
        Output.println("Question:");
        Output.println(getPrompt());
        Output.println("\nResponses:");
        Output.outputResponses(this.responses);

        // tabulate single response answers
        if (numResponses == 1) {
            int i;
            // initialie
            for (i = 0; i < choices.size(); i++) {
                tab.put(toAlphabetic(i), 0);
            }
            // tabulate
            for (Answer response : this.responses) {
                tab.put(response.getAnswer(), tab.get(response.getAnswer()) + 1);
            }
        // tabulate multiple response answers
        } else {
            for (Answer response: this.responses){
                if (tab.get(response.getAnswer()) == null){
                    tab.put(response.getAnswer(), 0);
                }
                tab.put(response.getAnswer(), tab.get(response.getAnswer()) + 1);
            }
        }
        // output tabulation
        Output.println("\nTabulation:");
        this.display();
        Output.outputTabulation(tab);
        Output.println("");
    }


    /***** Methods for serialization and deserialization ************************************/

    /**
     * These methods are based on example of serialization from student repo VehicleCityVersion project
     * */

    /**
     * Saves a survey.Question and all associated attributes in the proper location using the
     * built-in Serialization API
     * @return The path to the file the survey.Question is saved in
     */
    public String serialize(String name){

        return SerializationHelper.serialize(MultipleChoiceQuestion.class, this, basePath, name);
    }

    /**
     * Deserializes a specific survey.MultipleChoiceQuestion. The user will be presented with available MultipleChoiceQuestions to
     * deserialize.
     * @return The deserialized survey.MultipleChoiceQuestion
     */
    public static MultipleChoiceQuestion deserialize() {

        // Use the existing utility function to allow the user to pick the survey.MultipleChoiceQuestion from a
        // list of existing serialized MultipleChoiceQuestions
        String selectedQuestion = FileUtils.listAndPickFileFromDir(basePath);

        // Use the existing deserialization function to handle it from here
        return deserialize(selectedQuestion);
    }

    /**
     * Deserializes a survey.MultipleChoiceQuestion that can be found at the given path
     * @param path The path to the car
     * @return The deserialized car
     */
    public static MultipleChoiceQuestion deserialize(String path){

        String fullPath = basePath + path + File.separator;

        return SerializationHelper.deserialize(MultipleChoiceQuestion.class, fullPath);
    }


}
