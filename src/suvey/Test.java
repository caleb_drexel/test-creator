package suvey;

import utils.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Test extends Survey{

    private static final long serialVersionUID = 1L;
    public static final String SERIAL_BASE_PATH = FSConfig.serialDir + "Test" + File.separator;


    /**
     * Get test name
     * */
    @Override
    public void generateName(){
        String name;

        do{
            Output.println("Enter test name:");
            name = Input.readLine();
        }while(!this.validateName(name));

        this.name = name;
    }

    // add question and set it's correct answer
    @Override
    public void addQuestion(Question question){
        super.addQuestion(question);

        question.setCorrectAnswer();
    }

    // load serialized test
    public static Test load(String name){
        Output.println("Loading Test!");
        return deserialize(name);
    }

    public void displayWithoutAnswers(){
        this.display();
    }


    /**
     * Display test with answers below each question
     *
     * */
    public void displayWithAnswers(){
        int i;
        String divider = "____________________________________________________________\n";

        Output.println(divider);


        for (i=0; i<this.questions.size();i++){
            // display question
            System.out.print((i + 1) + ") ");
            this.questions.get(i).display();

            // skip answers if they do not exist
            if (this.questions.get(i).getCorrectAnswer() == null){
                continue;
            }
            // display  answers
            this.questions.get(i).displayCorrectAnswer();
        }

        Output.println(divider);

    }


    /**
     * modify question in test
     * */
    public void modifyQuestion () {

        int questionIdx;
        boolean response;
        Question question;

        Output.println("Which question would you like to modify?");
        questionIdx = Input.readIntInRange(1, this.questions.size())-1;
        question = this.questions.get(questionIdx);
        question.modify();

        // check to see if correct answer is still possible
        if (!question.validateCorrectAnswer(question.getCorrectAnswer())){
            Output.println("You must modify correct answer. Current correct answer is no longer valid!");
        } else {
            // give the option to change correct answer
            Output.println("Would you like to modify the correct answer?");
            response = Input.yesNoToTrueFalse();
            //return if no
            if (!response){
                return;
            }
        }

        question.setCorrectAnswer();

    }

    /**
     * Grade a test response. Ignore essay questions. Display results
     *
     * @param response A survey response for this test
     * */
    public void grade(SurveyResponse response){

        int essayQuestionCount = 0;
        int correctAnswerCount = 0;
        int totalQuestions = questions.size();
        Question question;
        List<Answer> answers = response.getResponses();
        Answer answer;

        Output.println("Grading!");
        for (int i=0; i<totalQuestions; i++){
            question = questions.get(i);
            answer = answers.get(i);

            // count essay questions
            if (question.getClass().equals(EssayQuestion.class)){
                essayQuestionCount +=1;
                continue;
            }

            // count correct questions
            if (question.getCorrectAnswer().equals(answer)){
                correctAnswerCount +=1;
            }

        }


        // output grading results
        System.out.printf("You received an %,.2f on the test. The test was worth 100 points.\n", ((double)correctAnswerCount/(double)totalQuestions)*100.0);
        if (essayQuestionCount!=0){
        System.out.printf("However only %,.2f those points could be auto " +
                "graded because there was %d essay question(s).\n",((double)(totalQuestions-essayQuestionCount)/(double)totalQuestions)*100.0, essayQuestionCount);
        }
        Output.println("");

    }


    /***** Methods for serialization and deserialization ************************************/

    /**
     * These methods are based on example of serialization from student repo VehicleCityVersion project
     * */

    /**
     * Saves a survey.Test and all associated attributes in the proper location using the
     * built-in Serialization API
     * @return The path to the file the survey.Test is saved in
     */
    public String serialize(String name){

        return SerializationHelper.serialize(Test.class, this, SERIAL_BASE_PATH, name);
    }

    /**
     * Deserializes a specific survey.Test. The user will be presented with available Tests to
     * deserialize.
     * @return The deserialized survey.Test
     */
    public static Test deserialize() {

        // Use the existing utility function to allow the user to pick the survey.Test from a
        // list of existing serialized Tests
        String selectedTest = FileUtils.listAndPickFileFromDir(SERIAL_BASE_PATH);

        // Use the existing deserialization function to handle it from here
        return SerializationHelper.deserialize(Test.class,  selectedTest);
    }

    /**
     * Deserializes a survey.Test that can be found at the given path
     * @param path The path to the car
     * @return The deserialized car
     */
    public static Test deserialize(String path){


        return SerializationHelper.deserialize(Test.class,  path);
    }

    public void grade() {
    }
}
