package suvey;

import utils.FileUtils;
import utils.Input;
import utils.Output;
import utils.SerializationHelper;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


public class EssayQuestion extends Question{

    private Integer numResponses;
    private final Integer MAX_RESPONSES = 100;
    private final Integer MIN_RESPONSES = 1;


    /**
     * Displays formatted question to standard out
     * */
    @Override
    public void display() {
        Output.println(this.toString());
    }


    /**
     * Run the process of setting up question object with through standard in
     * */
    @Override
    public void build() {

        //get prompt
        Output.println("Enter the prompt for your essay question:");
        this.setPrompt(Input.readLine());
        //get allowed number of responses
        Output.println("Enter the expected number of responses to this prompt ("+ MIN_RESPONSES + "-" + MAX_RESPONSES + ")");
        this.numResponses = Input.readIntInRange(MIN_RESPONSES, MAX_RESPONSES);

    }


    /**
     * Run the process of asking through std out and getting
     * a user response from std in
     *
     * @return Question response formatted into a single string
     * */
    @Override
    public Answer ask() {

        StringBuilder response = new StringBuilder();
        Answer answer;
        int i;
        this.display();

        //run for the allowed amount of responses
        for (i=0; i< numResponses; i++){

            if (i>0){
                response.append("\n");
            }
            response.append(Input.readLine());

        }
        answer = new Answer(response.toString());
        return answer;
    }


    /**
     * Tabulate all the essay questions
     * */
    public void tabulateAnswers(){

        // output responses
        Output.println("Question:");
        Output.println(getPrompt());
        Output.println("\nResponses:");
        Output.outputSpacedResponses(this.responses);

        // output tabulation (not really any tabulation here)
        Output.println("\nTabulation:");
        this.display();

        Output.outputSpacedResponses(this.responses);
        Output.println("");

    }

    @Override
    protected boolean validateCorrectAnswer(Answer answer) {
        return true;
    }

    @Override
    public void displayCorrectAnswer() {}

    /**
     * Run the process of modifying the current question through std io
     * */
    public void modify() {

        modifyPrompt();
        Output.println("Do you wish to modify the number of required responses? (yes or no)");

        if (!Input.yesNoToTrueFalse()){
            return;
        }
        Output.println("Enter the expected number of responses to this prompt ("+ MIN_RESPONSES + "-" + MAX_RESPONSES + ")");
        this.numResponses = Input.readIntInRange(MIN_RESPONSES, MAX_RESPONSES);

    }

    /**
     * Convert question to formatted string
     *
     * @return formatted question string
     * */
    @Override
    public String toString(){

        return this.getPrompt() +
                " (" +
                this.numResponses +
                " response(s) expected):" + "\n";
    }


    /***** Methods for serialization and deserialization ************************************/

    /**
     * These methods are based on example of serialization from student repo VehicleCityVersion project
     * */

    /**
     * Saves a survey.Question and all associated attributes in the proper location using the
     * built-in Serialization API
     * @return The path to the file the survey.Question is saved in
     */
    public String serialize(String name){

        return SerializationHelper.serialize(EssayQuestion.class, this, basePath, name);
    }

    /**
     * Deserializes a specific survey.EssayQuestion. The user will be presented with available EssayQuestions to
     * deserialize.
     * @return The deserialized survey.EssayQuestion
     */
    public static EssayQuestion deserialize() {

        // Use the existing utility function to allow the user to pick the survey.EssayQuestion from a
        // list of existing serialized EssayQuestions
        String selectedQuestion = FileUtils.listAndPickFileFromDir(basePath);

        // Use the existing deserialization function to handle it from here
        return deserialize(selectedQuestion);
    }

    /**
     * Deserializes a survey.EssayQuestion that can be found at the given path
     * @param path The path to the car
     * @return The deserialized car
     */
    public static EssayQuestion deserialize(String path){

        String fullPath = basePath + path + File.separator;

        return SerializationHelper.deserialize(EssayQuestion.class, fullPath);
    }

}
