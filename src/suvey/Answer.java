package suvey;

import utils.FSConfig;
import utils.FileUtils;
import utils.SerializationHelper;

import java.io.File;
import java.io.Serializable;
import java.util.*;

public class Answer implements Serializable {

    private String answer;
    protected static final long serialVersionUID = 1L;
    private static final String basePath = FSConfig.serialDir + "Answer" + File.separator;

    Answer(String answer){
        this.answer = answer;
    }

    public String getAnswer(){
        return this.answer;
    }

    public void setAnswer(String answer){
        this.answer = answer;
    }

    @Override
    public boolean equals(Object o) {


        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Answer answer1 = (Answer) o;

        Set<String> answerList = new HashSet<>(Arrays.asList(answer.split("\n")));
        Set<String> answer1List = new HashSet<>(Arrays.asList(answer1.getAnswer().split("\n")));

        return Objects.equals(answerList, answer1List);
    }

    @Override
    public int hashCode() {
        return Objects.hash(answer);
    }

    /***** Methods for serialization and deserialization ************************************/

    /**
     * These methods are based on example of serialization from student repo VehicleCityVersion project
     * */

    /**
     * Saves an Answer and all associated attributes in the proper location using the
     * built-in Serialization API
     * @return The path to the file the Answer is saved in
     */
    public String serialize(String name){

        return SerializationHelper.serialize(Answer.class, this, basePath, name);
    }

    /**
     * Deserializes a specific suvey.Answer. The user will be presented with available Answers to
     * deserialize.
     * @return The deserialized suvey.Answer
     */
    public static Answer deserialize() {

        // Use the existing utility function to allow the user to pick the suvey.Answer from a
        // list of existing serialized Answer
        String selectedAnswer = FileUtils.listAndPickFileFromDir(basePath);

        // Use the existing deserialization function to handle it from here
        return SerializationHelper.deserialize(Answer.class,  selectedAnswer);
    }

    /**
     * Deserializes a suvey.Answer that can be found at the given path
     * @param path The path to the car
     * @return The deserialized car
     */
    public static Answer deserialize(String path){

        String fullPath = basePath + path + File.separator;

        return SerializationHelper.deserialize(Answer.class,  fullPath);
    }
}
