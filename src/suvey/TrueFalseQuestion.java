package suvey;

import utils.FileUtils;
import utils.Input;
import utils.Output;
import utils.SerializationHelper;

import java.io.File;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


public class TrueFalseQuestion extends MultipleChoiceQuestion {


    /**
     * Displays formatted question to standard out
     * */
    @Override
    public void display() {

        Output.println(this.toString());
    }


    /**
     * Run the process of setting up question object with through standard in
     * */
    @Override
    public void build() {

        Output.println("Enter the prompt for your True/False question:");
        this.setPrompt(Input.readLine());
        choices.add("True");
        choices.add("False");

    }


    /**
     * Run the process of asking through std out and getting
     * a user response from std in
     *
     * @return Question response formatted into a single string
     * */
    @Override
    public Answer ask() {
        String response;
        Answer answer;

        //get user input until valid input is received
        do {
            this.display();
            response = Input.readLine().toLowerCase(Locale.ROOT);

            //accept different formats of true and false
            if (response.equals("t")){
                response = "true";
            } else if (response.equals("f")){
                response = "false";
            }

        } while (!validateResponse(response));

        answer = new Answer(response);
        return answer;
    }

    @Override
    public void displayCorrectAnswer(){

        if (this.correctAnswer==null){
            Output.println("No correct answer set\n");
            return;
        }

        Output.println("The correct answer is " + this.correctAnswer.getAnswer() + "\n");
    }

    /**
     * Run the process of modifying the current question through std io
     * */
    @Override
    public void modify(){
        this.modifyPrompt();
    }
    public boolean validateResponse(String response){

        if (response.equals("true") ||
                response.equals("false")) {
            return true;
        }

        Output.println("Invalid Input");
        return false;

    }



    /**
     * Validate correct answer is possible choice
     *
     * @return indicates if a correct answer is valid
     * */
    @Override
    protected boolean validateCorrectAnswer(Answer answer) {

        if (answer == null){return false;}
        return validateResponse(answer.getAnswer());

    }


    /**
     * Convert question to formatted string
     *
     * @return formatted question string
     * */
    @Override
    public String toString (){
        return this.getPrompt()+" ('True' or 'False')"+":"+ "\n";
    }


    @Override public void displayPrompt(){
        this.display();
    }

    public String capFirst(String str){
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }


    /**
     * Tabulate all the TF questions
     * */
    public void tabulateAnswers(){

        // output responses
        Map<String, Integer> tab = new HashMap<>();
        Output.println("Question:");
        Output.println(getPrompt());
        Output.println("\nResponses:");
        Output.outputResponses(this.responses);

        // initialize
        tab.put("True", 0);
        tab.put("False", 0);

        // tabulate
        for(Answer response : this.responses){
            tab.put(capFirst(response.getAnswer()), tab.get(capFirst(response.getAnswer()))+1);
        }

        // output tabulation
        Output.println("\nTabulation:");
        this.display();
        Output.outputTabulation(tab);
        Output.println("");

    }


    /***** Methods for serialization and deserialization ************************************/

    /**
     * These methods are based on example of serialization from student repo VehicleCityVersion project
     * */

    /**
     * Saves a survey.Question and all associated attributes in the proper location using the
     * built-in Serialization API
     * @return The path to the file the survey.Question is saved in
     */
    public String serialize(String name){

        return SerializationHelper.serialize(TrueFalseQuestion.class, this, basePath, name);
    }

    /**
     * Deserializes a specific survey.TrueFalseQuestion. The user will be presented with available TrueFalseQuestions to
     * deserialize.
     * @return The deserialized survey.TrueFalseQuestion
     */
    public static TrueFalseQuestion deserialize() {

        // Use the existing utility function to allow the user to pick the survey.TrueFalseQuestion from a
        // list of existing serialized TrueFalseQuestions
        String selectedQuestion = FileUtils.listAndPickFileFromDir(basePath);

        // Use the existing deserialization function to handle it from here
        return deserialize(selectedQuestion);
    }

    /**
     * Deserializes a survey.TrueFalseQuestion that can be found at the given path
     * @param path The path to the car
     * @return The deserialized car
     */
    public static TrueFalseQuestion deserialize(String path){

        String fullPath = basePath + path + File.separator;

        return SerializationHelper.deserialize(TrueFalseQuestion.class, fullPath);
    }

}
