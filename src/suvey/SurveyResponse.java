package suvey;

import utils.FSConfig;
import utils.FileUtils;
import utils.Output;
import utils.SerializationHelper;

import java.io.File;
import java.io.Serializable;
import java.util.List;


public class SurveyResponse implements Serializable {

    private static final long serialVersionUID = 1L;
    public String serial_base_path;
    private final List<Answer> responses;
    private final Survey survey;



    SurveyResponse(Survey survey, List<Answer> responses){

        this.responses = responses;
        this.survey = survey;
        this.serial_base_path = generateSerialBasePath(survey);
    }

    public List<Answer> getResponses(){return this.responses;}
    public static String generateSerialBasePath(Survey survey) {

        if (survey.getClass().equals(Test.class)){
            return Test.SERIAL_BASE_PATH + "Responses" + File.separator + survey.getName() + File.separator;
        }
        return Survey.SERIAL_BASE_PATH + "Responses" + File.separator + survey.getName() + File.separator;
    }

    /**
     * Convert question to formatted string
     *
     * @return formatted question string
     * */
    @Override
    public String toString(){
        StringBuilder responseString = new StringBuilder();
        List<Question> questions = survey.getQuestions();
        int i;


        for (i=0; i<responses.size(); i++){
            responseString.append(i+1).append(") ");
            responseString.append(questions.get(i).toString()).append("\n\n");
            responseString.append("Response:\n");
            responseString.append(responses.get(i).getAnswer()).append("\n\n");
        }

        return responseString.toString();
    }

    /**
     * Save survey results to a formatted text file
     *
     * */
    public void saveStringFile(){

        int fileCount = 0;
        String basePath;
        if (survey.getClass().equals(Test.class)){
            basePath=  FSConfig.responseDir + "Test" + File.separator +  survey.getName() + File.separator;
        } else {
            basePath=  FSConfig.responseDir + "Survey" + File.separator +  survey.getName() + File.separator;
        }

        FileUtils.createDirectory(basePath);


        try {
            //catch if empty directory and keep at 0
            fileCount = FileUtils.getAllFilePathsInDir(basePath).size();
        } catch (IllegalStateException ignored){}

        String path = basePath + "response_"+fileCount + File.separator;
        FileUtils.writeNewFile(path, this.toString());
    }

    public void save(){

        int fileCount = 0;
        String name;
        FileUtils.createDirectory(this.serial_base_path);


        try {
            //catch if empty directory and keep at 0
            fileCount = FileUtils.getAllFilePathsInDir(this.serial_base_path).size();
        } catch (IllegalStateException ignored){}

        name = "response_"+fileCount;
        this.serialize(name);
    }

    public static SurveyResponse load(String path){
        Output.println("Loading response!");
        return deserialize(path);
    }

    /***** Methods for serialization and deserialization ************************************/

    /**
     * These methods are based on example of serialization from student repo VehicleCityVersion project
     * */

    /**
     * Saves a survey.SurveyResponse and all associated attributes in the proper location using the
     * built-in Serialization API
     * @return The path to the file the survey.SurveyResponse is saved in
     */
    public String serialize(String name){

        return SerializationHelper.serialize(SurveyResponse.class, this, serial_base_path, name);
    }

    /**
     * Deserializes a survey.SurveyResponse that can be found at the given path
     * @param path The path to the car
     * @return The deserialized car
     */
    public static SurveyResponse deserialize(String path){


        return SerializationHelper.deserialize(SurveyResponse.class,  path);
    }

}
