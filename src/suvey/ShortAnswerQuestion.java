package suvey;

import utils.FileUtils;
import utils.Input;
import utils.Output;
import utils.SerializationHelper;

import java.io.File;
import java.util.*;


public class ShortAnswerQuestion extends EssayQuestion {

    private Integer maxResponseSize;
    private final Integer MIN_RESPONSE_LEN = 50;
    private final Integer MAX_RESPONSE_LEN = 1000;


    /**
     * Displays formatted question to standard out
     * */
    @Override
    public void display() {
        Output.println(this.toString());
    }


    /**
     * Run the process of setting up question object with through standard in
     * */
    @Override
    public void build() {

        Output.println("Enter the prompt for your short answer question:");
        this.setPrompt(Input.readLine());
        Output.println("Enter the max char size of response:");
        this.maxResponseSize = Input.readIntInRange(MIN_RESPONSE_LEN, MAX_RESPONSE_LEN);

    }


    /**
     * Run the process of asking through std out and getting
     * a user response from std in
     *
     * @return Question response formatted into a single string
     * */
    @Override
    public Answer ask() {
        String response;
        Answer answer;

        //get user input until valid input is received
        do{
            this.display();
            response = Input.readLine();
        } while (!validateResponse(response));

        answer = new Answer(response);
        return answer;
    }


    /**
     * Run the process of modifying the current question through std io
     * */
    @Override
    public void modify() {

        modifyPrompt();
        Output.println("Do you wish to modify the max chars? (yes or no)");

        if (!Input.yesNoToTrueFalse()){
            return;
        }

        Output.println("Enter the max char size of response:");
        this.maxResponseSize = Input.readIntInRange(MIN_RESPONSE_LEN, MAX_RESPONSE_LEN);

    }


    /**
     * Convert question to formatted string
     *
     * @return formatted question string
     * */
    @Override
    public String toString(){
        return this.getPrompt() + "\n" +
                "(short answer " +
                this.maxResponseSize +
                " max chars):" + "\n";
    }


    /**
     * Convert question to formatted string
     *
     * @return indicates if question response string is valid
     * */
    protected boolean validateResponse(String response){

        if (response.length() > this.maxResponseSize){
            Output.println("Response exceeds max size " + this.maxResponseSize);
            return false;
        }
        return true;
    }

    @Override
    public void displayCorrectAnswer(){

        if (this.correctAnswer==null){
            Output.println("No correct responses set\n");
            return;
        }

        Output.println("The correct response(s):\n" + this.correctAnswer.getAnswer() + "\n");
    }



    /**
     * Validate correct answer is possible choice
     *
     * @return indicates if a correct answer is valid
     * */
    @Override
    protected boolean validateCorrectAnswer(Answer answer) {

        if (answer == null){return false;}
        return validateResponse(answer.getAnswer());

    }


    /**
     * Tabulate all the short answer questions
     * */
    public void tabulateAnswers(){

        Set<String> choices = new HashSet<String>();
        Map<String, Integer> tab = new HashMap<>();
        Output.println("Question:");
        Output.println(getPrompt());
        Output.println("\nResponses:");
        Output.outputResponses(this.responses);

        // initialize choices
        for(Answer response : this.responses){
            choices.add(response.getAnswer());
        }

        // set choices to 0
        for(String choice : choices){
            tab.put(choice, 0);
        }

        // tabulate
        for(Answer response : this.responses){
            tab.put(response.getAnswer(), tab.get(response.getAnswer())+1);
        }

        // output tabulation
        Output.println("\nTabulation:");
        this.display();
        tab.forEach((k,v) -> Output.println(k + " " + v));
        Output.println("");

    }
    /***** Methods for serialization and deserialization ************************************/

    /**
     * These methods are based on example of serialization from student repo VehicleCityVersion project
     * */

    /**
     * Saves a survey.Question and all associated attributes in the proper location using the
     * built-in Serialization API
     * @return The path to the file the survey.Question is saved in
     */
    public String serialize(String name){

        return SerializationHelper.serialize(ShortAnswerQuestion.class, this, basePath, name);
    }

    /**
     * Deserializes a specific survey.ShortAnswerQuestion. The user will be presented with available ShortAnswerQuestions to
     * deserialize.
     * @return The deserialized survey.ShortAnswerQuestion
     */
    public static ShortAnswerQuestion deserialize() {

        // Use the existing utility function to allow the user to pick the survey.ShortAnswerQuestion from a
        // list of existing serialized ShortAnswerQuestions
        String selectedQuestion = FileUtils.listAndPickFileFromDir(basePath);

        // Use the existing deserialization function to handle it from here
        return deserialize(selectedQuestion);
    }

    /**
     * Deserializes a survey.ShortAnswerQuestion that can be found at the given path
     * @param path The path to the car
     * @return The deserialized car
     */
    public static ShortAnswerQuestion deserialize(String path){

        String fullPath = basePath + path + File.separator;

        return SerializationHelper.deserialize(ShortAnswerQuestion.class, fullPath);
    }

}
