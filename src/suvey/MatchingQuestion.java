package suvey;

import utils.*;

import java.io.File;
import java.util.*;

public class MatchingQuestion extends Question{

    private Integer numChoices;
    private final List<String> firstColumn = new ArrayList<>();
    private final List<String> secondColumn = new ArrayList<>();
    private static final Integer DISPLAY_COLUMN_SPACE = 10;
    private static final Integer MIN_CHOICES = 2;
    private static final Integer MAX_CHOICES = 50;


    /**
     * Displays formatted question to standard out
     * */
    @Override
    public void display() {

        Output.println(this.toString());

    }


    /**
     * Run the process of setting up question object with through standard in
     * */
    @Override
    public void build() {

        int i;

        Output.println("Enter the prompt for your matching question:");
        this.setPrompt(Input.readLine());
        Output.println("Enter the number of choices for matching question.");
        this.numChoices = Input.readIntInRange(MIN_CHOICES,MAX_CHOICES);

        for (i=1;i<this.numChoices+1; i++){
            Output.println(getPrompt() + ", Enter left choice #" + i);
            firstColumn.add(Input.readLine());
        }

        for (i=1;i<this.numChoices+1; i++){
            Output.println(getPrompt() + ", Enter right choice #" + i);
            secondColumn.add(Input.readLine());
        }

    }


    /**
     * Run the process of asking through std out and getting
     * a user response from std in
     *
     * @return Question response formatted into a single string
     * */
    @Override
    public Answer ask() {

        StringBuilder fullResponse = new StringBuilder();
        String response;
        Answer answer;
        int i, firstColEndIdx;
        List<String> firstColumnUsed = new ArrayList<>();
        List<String> secondColumnUsed = new ArrayList<>();

        this.display();

        for (i=0;i<this.numChoices; i++){
            do{
                response = Input.readLine().toUpperCase(Locale.ROOT);
            }while(!this.validateResponse(response, firstColumnUsed, secondColumnUsed));

            fullResponse.append(response);

            firstColEndIdx = getFirstColIdx(response);
            firstColumnUsed.add(response.substring(0, firstColEndIdx));
            secondColumnUsed.add(response.substring(firstColEndIdx));

            if (i<this.numChoices-1){
                fullResponse.append("\n");
            }

        }
        answer = new Answer(fullResponse.toString());
        return answer;
    }


    /**
     * Run the process of modifying the current question through std io
     * */
    public void modify() {

        String response;

        this.modifyPrompt();

        Output.println("Do you want to modify choices? (yes or no)");
        displayChoices();

        if (!Input.yesNoToTrueFalse()) {
            return;
        }


        while (true) {
            Output.println("Which choice do you want to modify?:");
            displayChoices();

            do {
                response = Input.readLine().toUpperCase(Locale.ROOT);
            } while (!this.validateModify(response));

            Output.println("Choice is entered. Enter new value:");

            if (Validation.isInt(response)){
                this.secondColumn.set(Integer.parseInt(response), Input.readLine());
            } else {
                this.firstColumn.set(alphabeticToInt(response), Input.readLine());
            }

            Output.println("Choice modified!");
            Output.println("Would you like to modify another choice? (yes or no)");

            if (!Input.yesNoToTrueFalse()){
                return;
            }

        }
    }


    /**
     * Convert question to formatted string
     *
     * @return formatted question string
     * */
    @Override
    public String toString(){
        return this.getPrompt() + ":\n" +
                generateChoicesStr() + "\n";
    }


    /**
     * Turn choices into formatted string
     *
     * @return formatted choices string
     * */
    public String generateChoicesStr(){

        int i;
        StringBuilder output = new StringBuilder();
        String strOffset = String.valueOf(getOffset());

        for (i=0; i<this.numChoices; i++){
            output.append(String.format("\t%s) %-"+strOffset+"s%d) %-15s%n", Question.toAlphabetic(i), this.firstColumn.get(i), i, this.secondColumn.get(i)));
        }

        return output.toString();

    }

    /**
     * Calculates the offset of the first column so the second column is apparently spaced
     * Gets the max chars in the first column choices
     *
     * @return max chars of first column options
     * */
    private int getOffset(){

        int i;
        int max = 0;


        for (i=0; i<this.numChoices; i++){
            max = Math.max(max, this.firstColumn.get(i).length());
        }

        return max+DISPLAY_COLUMN_SPACE;
    }

    //display just the choices
    public void displayChoices(){
        Output.println(generateChoicesStr());
    }


    /**
     * Parses matching response and returns first col idx
     *
     * @param response right and left column values from match
     * @return values of first column idx
     * */
    private int getFirstColIdx(String response){
        char c;
        int i;
        int strLen = response.length();
        int firstColEndIdx = 0;

        for (i=0; i<strLen; i++) {
            c = response.charAt(i);
            if (c < 'A' || c > 'Z'){
                firstColEndIdx = i;
                break;
            }
        }
        return firstColEndIdx;
    }


    private boolean validateModify(String response){

        if ((Validation.isInt(response) && !validateSecondColResponse(response))
         || (!Validation.isInt(response) && !validateFirstColResponse(response))){
            Output.println("Invalid Input");
            return false;
        }

        return true;

    }


    /**
     * Convert question to formatted string
     *
     * @return indicates if question response string is valid
     * */
    public boolean validateResponse(String response,List<String> firstColumnUsed, List<String> secondColumnUsed){
        String errorMessage = "Invalid Input";

        int firstColEndIdx = getFirstColIdx(response);
        String firstColumn, secondColumn;



        firstColumn = response.substring(0, firstColEndIdx);
        secondColumn = response.substring(firstColEndIdx);


        if (!validateFirstColResponse(firstColumn) ||
                !validateSecondColResponse(secondColumn)
            )
        {
            Output.println(errorMessage);
            return false;
        }

        if (firstColumnUsed.contains(firstColumn)){
            Output.println(errorMessage + " - "+firstColumn+" already used");
            return false;
        }

        if(secondColumnUsed.contains(secondColumn)){
            Output.println(errorMessage + " - "+secondColumn+" already used");
            return false;
        }

        return true;
    }

    //validates second col response val
    private boolean validateSecondColResponse(String secondColumn) {

        return Validation.isInt(secondColumn) &&
                (Integer.parseInt(secondColumn) < numChoices) &&
                (Integer.parseInt(secondColumn) >= 0) &&
                !secondColumn.equals("-0");
    }

    //validates first col response val
    private boolean validateFirstColResponse(String response){

        int strLen = response.length();
        int numVal;
        int i;
        char c;

        for (i=0; i<strLen; i++){
            c = response.charAt(i);
            if (c < 'A' || c > 'Z'){
                return false;
            }
        }

        numVal = alphabeticToInt(response);

        return numVal <= this.numChoices - 1;

    }

    @Override
    public void displayCorrectAnswer(){

        if (this.correctAnswer==null){
            Output.println("No correct combination set\n");
            return;
        }

        Output.println("The correct combination is:\n" + this.correctAnswer.getAnswer() + "\n");
    }


    /**
     * Tabulate all the matching questions
     * */
    public void tabulateAnswers(){

        Set<String> choices = new HashSet<String>();
        Map<String, Integer> tab = new HashMap<>();
        Output.println("Question:");
        Output.println(getPrompt());
        Output.println("\nResponses:");
        Output.outputResponses(this.responses);


        for(Answer response : this.responses){
            choices.add(response.getAnswer());
        }

        // initialize choices to 0
        for(String choice : choices){
            tab.put(choice, 0);
        }

        // tabulate
        for(Answer response : this.responses){
            tab.put(response.getAnswer(), tab.get(response.getAnswer())+1);
        }

        // output tabulation
        Output.println("\nTabulation:");
        this.display();
        tab.forEach((k,v) -> Output.println(v + "\n" + k + "\n"));
        Output.println("");

    }


    /**
     * Validate correct answer is possible choice
     *
     * @return indicates if a correct answer is valid
     * */
    @Override
    protected boolean validateCorrectAnswer(Answer answer) {
        int firstColEndIdx;
        if (answer == null){return false;}
        String responseStr = answer.getAnswer();
        List<String> firstColumnUsed = new ArrayList<>();
        List<String> secondColumnUsed = new ArrayList<>();
        List<String> matches = new ArrayList<>(Arrays.asList(responseStr.split("\n")));


        for (String match: matches){

            if (!validateResponse(match, firstColumnUsed, secondColumnUsed)){
                return false;
            }

            firstColEndIdx = getFirstColIdx(responseStr);
            firstColumnUsed.add(responseStr.substring(0, firstColEndIdx));
            secondColumnUsed.add(responseStr.substring(firstColEndIdx));
        }

        return true;
    }


    /***** Methods for serialization and deserialization ************************************/

    /**
     * These methods are based on example of serialization from student repo VehicleCityVersion project
     * */

    /**
     * Saves a suvey.Question and all associated attributes in the proper location using the
     * built-in Serialization API
     * @return The path to the file the suvey.Question is saved in
     */
    public String serialize(String name){

        return SerializationHelper.serialize(MatchingQuestion.class, this, basePath, name);
    }

    /**
     * Deserializes a specific suvey.MatchingQuestion. The user will be presented with available MatchingQuestions to
     * deserialize.
     * @return The deserialized suvey.MatchingQuestion
     */
    public static MatchingQuestion deserialize() {

        // Use the existing utility function to allow the user to pick the suvey.MatchingQuestion from a
        // list of existing serialized MatchingQuestions
        String selectedQuestion = FileUtils.listAndPickFileFromDir(basePath);

        // Use the existing deserialization function to handle it from here
        return deserialize(selectedQuestion);
    }

    /**
     * Deserializes an suvey.MatchingQuestion that can be found at the given path
     * @param path The path to the car
     * @return The deserialized car
     */
    public static MatchingQuestion deserialize(String path){

        String fullPath = basePath + path + File.separator;

        return SerializationHelper.deserialize(MatchingQuestion.class, fullPath);
    }

}
