import suvey.*;
import utils.Input;
import utils.Output;

import java.util.List;

public abstract class MainMenu {


    protected List<String> commands;
    //Main menu options

    public abstract void create();
    public abstract void take();
    public abstract void modify();
    public abstract void save();
    public abstract void load();
    public abstract void tabulate();
    public abstract void runMainMenuCommand(int command);

    /**
     * Main loop for running the main menu. Runs the menu until the user quits
     *
     * */
    public void run(){

        int command;

        while(true){
            command = OpeningMenu.getMenuCommand(this.commands);

            //quit command
            if (command == this.commands.size()-1){
                break;
            }
            runMainMenuCommand(command);
        }

    }


}
