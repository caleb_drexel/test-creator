package utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;

/**
 * @author Sean Grimes, sean@seanpgrimes.com
 *
 * File utility class.
 *
 * This is pulled from student repo VehicalCityVersion project
 */
@SuppressWarnings({"UnusedReturnValue", "unused"})
public class FileUtils {

    //https://stackoverflow.com/questions/893977/java-how-to-find-out-whether-a-file-name-is-valid
    public static final List<Character> ILLEGAL_CHARACTERS = new ArrayList<>(Arrays.asList( '/', '\n', '\r', '\t', '\0', '\f', '`', '?', '*', '\\', '<', '>', '|', '\"', ':' ));


    public static boolean validateFileName(String name){

        int i;
        int strLen = name.length();
        char c;

        for (i=0; i<strLen; i++) {
            c = name.charAt(i);
            if (FileUtils.ILLEGAL_CHARACTERS.contains(c)){
                Output.println("Invalid char in file name");
                return false;
            }
        }
        return true;
    }


    /**
     * Append data to an existing file
     * @param filename Path to the file
     * @param str The string to write to the new file
     */
    public static boolean appendToFile(String filename, String str){
        BufferedWriter writer;
        try{
            writer = Files.newBufferedWriter(Paths.get(filename),
                    StandardOpenOption.APPEND);
            writer.write(str);
            writer.close();
        }
        catch(IOException e){
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * Write a new file to disk. Will overwrite existing file
     * @param fileName Path to the file
     * @param str The string to write to the file
     */
    public static boolean writeNewFile(String fileName, String str){
        BufferedWriter writer;
        try {
            writer = Files.newBufferedWriter(Paths.get(fileName));
            writer.write(str);
            writer.close();
        }
        catch(IOException e){
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * Read a file line by line
     * @param filePath The path to the file
     * @return A list of lines of the file (as strings) if file is found and readable
     */
    public static List<String> readLineByLine(String filePath) {
        List<String> lines = new ArrayList<>();
        BufferedReader br = null;
        try{
            br = Files.newBufferedReader(Paths.get(filePath));
            String line = br.readLine();
            while(line != null){
                lines.add(line);
                line = br.readLine();
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally{
            if(br != null){
                try{
                    br.close();
                }
                catch(IOException e){
                    System.err.println("Failed to finalize readLineByLine");
                }
            }
        }
        return lines;
    }

    /**
     * Create a directory if one does not exist
     * @param directoryPath The path to the directory to be created
     * @return True if success, else false
     */
    public static boolean createDirectory(String directoryPath){
        File dir = new File(directoryPath);
        // Nothing exists here, create the directory and all parent directories
        if(!dir.exists())
            return dir.mkdirs();

        // Something exists at the supplied path, see if it's a directory. If it is,
        // return true. If it's not, it's something else so return false.
        return dir.isDirectory();
    }

    /**
     * Get a sorted list of all files in a directory
     * @param path The path to the directory
     * @return The sorted list of paths
     */
    public static List<String> getAllFilePathsInDir(String path){
        List<String> paths = new ArrayList<>();
        File[] files = new File(path).listFiles();
        if(files == null || files.length == 0)
            throw new IllegalStateException(path + " is empty");
        for(File f : files){
            if(f.isFile())
                paths.add(f.getAbsolutePath());
        }
        return sortPaths(paths);
    }

    /**
     * Get a sorted list of all files in a directory
     * @param path The path to the directory
     * @return The sorted list of file names
     */
    public static List<String> getAllFileNamesInDir(String path){
        List<String> paths = getAllFilePathsInDir(path);
        List<String> fileNames = new ArrayList<>();
        int i;
        Path file;

        for (i=0;i<paths.size();i++){
            file = Paths.get(paths.get(i));
            fileNames.add(file.getFileName().toString());
        }

        return fileNames;
    }

    /**
     * Sort strings *in place* using a custom comparator
     * @param paths The list of strings to be sorted
     * @return The sorted list of strings
     */
    private static List<String> sortPaths(List<String> paths){
        if(paths.isEmpty()) return Collections.emptyList();
        if(paths.size() == 1) return paths;
        Comparator<String> comp = Comparator.comparing((String x) -> x);
        paths.sort(comp);
        return paths;
    }

    /**
     * Displays a user a list of files available in a given directory. Allows user to
     * select a single file from this list of files
     * @param dirPath The path to the directory that holds the files
     * @return The absolute path to the selected file
     */
    public static String listAndPickFileFromDir(String dirPath){
        File dir = new File(dirPath);
        if(!dir.exists() || !dir.isDirectory())
            throw new IllegalStateException(dirPath + " is invalid");

        File[] files = dir.listFiles();
        if(files == null || files.length == 0)
            throw new IllegalStateException(dirPath + " is empty");

        for(int i = 0; i < files.length; ++i){
            if(files[i].isFile())
                // Adding 1 to avoid 0-indexed UI. getName to chop off full path.
                Output.println((i + 1) + ") " + files[i].getName());
        }

        // Get a valid integer between 1 and number of files
        int fileSelection = Input.readIntInRange(1, files.length);

        // Remember to subtract 1 to get back to 0-indexed value
        return files[fileSelection - 1].getAbsolutePath();
    }

    /**
     * Recursively deletes all files and folders in a directory
     * Function from https://www.baeldung.com/java-delete-directory
     *
     * @param dirPath The path to the root directory to be deleted
     * @return Indicates successful deletion
     */

    public static boolean deleteDirectory(String dirPath) {
        File directoryToBeDeleted = new File(dirPath);
        File[] allContents = directoryToBeDeleted.listFiles();
        if (allContents != null) {
            for (File file : allContents) {
                deleteDirectory(file);
            }
        }
        return directoryToBeDeleted.delete();
    }

    /**
     * Recursively deletes all files and folders in a directory
     * Function from https://www.baeldung.com/java-delete-directory
     *
     * @param directoryToBeDeleted The File of the root directory to be deleted
     * @return Indicates successful deletion
     */
    public static boolean deleteDirectory(File directoryToBeDeleted) {
        File[] allContents = directoryToBeDeleted.listFiles();
        if (allContents != null) {
            for (File file : allContents) {
                deleteDirectory(file);
            }
        }
        return directoryToBeDeleted.delete();
    }
}
