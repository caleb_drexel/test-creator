package utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Locale;

/**
 * @author Sean Grimes, sean@seanpgrimes.com
 *
 * Utility class for getting user input
 *
 * This is pulled from student repo VehicalCityVersion project
 */
@SuppressWarnings("WeakerAccess")
public class Input {
    // Validate input as an int between inclusive range
    public static int readIntInRange(int start, int end){
        String failSpeech = "Please enter a valid number between " + start + " - " + end;
        // BufferedReader is better than Scanner. Prove me wrong.
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String line = "z";  // Some non-integer value to fail parsing if checks are missed
        try{
            line = br.readLine();
            while(
                    line == null
                            || line.length() <= 0
                            || !Validation.isIntBetweenInclusive(start, end, line)
            ){
                Output.println(failSpeech);
                line = br.readLine();
            }
        }
        catch(IOException e){
            e.printStackTrace();
        }
        //noinspection ConstantConditions
        return Integer.parseInt(line);
    }


    public static String readLine(){
        String failSpeech = "Invalid input";

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String line = "";
        try{
            line = br.readLine();
            while(
                    line == null
                            || line.length() <= 0
            ){
                Output.println(failSpeech);
                line = br.readLine();
            }
        }
        catch(IOException e){
            e.printStackTrace();
        }
        return line;
    }



    public static boolean yesNoToTrueFalse(){
        String input;

        while (true) {
            input = Input.readLine();

            if (input.toLowerCase(Locale.ROOT).equals("yes")) {
                return true;
            } else if (input.toLowerCase(Locale.ROOT).equals("no")) {
                return false;
            }

            Output.println("Invalid input, please answer 'yes' or 'no'");
        }

    }

}
