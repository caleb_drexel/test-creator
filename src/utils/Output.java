package utils;

import suvey.Answer;

import java.util.List;
import java.util.Map;

public class Output {


    public static void outputOptions(List<String> options){

        int i;

        for(i=0;i<options.size();i++){
            Output.println("\t" + String.valueOf(i) + ") " + options.get(i));
        }

    }

    public static void outputResponses(List<Answer> responses){
        int i;

        for(i=0;i<responses.size();i++){
            Output.println(responses.get(i).getAnswer());
        }
    }

    public static void outputSpacedResponses(List<Answer> responses){

        for (Answer response : responses){
            Output.println(response.getAnswer()+"\n");
        }
    }


    public static void outputTabulation(Map<String, Integer> tab){

        tab.forEach((k,v) -> Output.println(k + ": " + v));

    }

    public static void println(String str){
        System.out.println(str);
    }
}
