import utils.Input;
import utils.Output;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class OpeningMenu {

    private MainMenu mainMenu;
    private final List<String> commands = new ArrayList<>(Arrays.asList(
            "Survey",
            "Test"));


    /**
     * Main loop for running the main menu. Runs the menu until the user quits
     *
     * */

    public void run(){

        int command;

        while(true){
            command = OpeningMenu.getMenuCommand(this.commands);

            runCommand(command);
        }

    }


    public void runCommand(int command){

        switch(command) {
            case 0: //Survey
                mainMenu = new SurveyMainMenu();
                break;
            case 1://Test
                mainMenu = new TestMainMenu();
                break;
        }

        mainMenu.run();

    }


    //display menu and get user response
    public static int getMenuCommand(List<String> options){

        int selected;

        Output.outputOptions(options);
        selected = Input.readIntInRange(0, options.size()-1);

        return selected;

    }

}
