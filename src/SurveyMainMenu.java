import suvey.*;
import utils.FileUtils;
import utils.Output;

import java.util.ArrayList;
import java.util.Arrays;

public class SurveyMainMenu extends MainMenu {

    private Survey survey;

    SurveyMainMenu(){

        // Survey menu commands
        commands = new ArrayList<>(Arrays.asList(
                "Create a new Survey",
                "Display an existing Survey",
                "Load an existing Survey",
                "Save the current Survey",
                "Take the current Survey",
                "Modifying the current Survey",
                "Tabulate a survey",
                "Quit"));
    }


    /**
     * Serialize and save survey object
     * */
    public void save(){
        if (this.survey == null){
            Output.println("You must have a survey loaded in order to save it.");
            return;
        }

        this.survey.save();
        display();
        Output.println("Survey Saved\n");
    }


    /**
     * load a serialized survey into memory
     * */
    public void load(){

        String selectedPath;

        Output.println("Pick a survey to load.");

        try {
            selectedPath = FileUtils.listAndPickFileFromDir(Survey.SERIAL_BASE_PATH);
        } catch (IllegalStateException e) {
            Output.println("You must save a survey before one can be loaded!\n");
            return;
        }

        this.survey= Survey.load(selectedPath);

    }


    /**
     * Run the creation of a new survey
     * */
    @Override
    public void create(){
        survey = new Survey();
        CreateMenu createMenu = new CreateMenu(survey);
        survey.generateName();
        createMenu.run();
    }


    /**
     * Run the process of taking a survey
     * */
    public void take(){
        if (this.survey == null){
            Output.println("You must have a survey loaded in order to take it.");
            return;
        }

        this.survey.take();
    }


    /**
     * Run the process of modifying a survey
     * */
    public void modify(){
        if (this.survey == null){
            Output.println("You must create or load a survey before it can be modified!");
            return;
        }

        this.survey.modifyQuestion();
    }


    /**
     * Display all the current surveys questions
     * */
    public void display(){
        if (this.survey == null){
            Output.println("You must have a survey loaded in order to display it.");
            return;
        }
        this.survey.display();
    }

    /**
     * Tabulate the current survey
     * */
    public void tabulate() {
        if (this.survey == null){
            Output.println("You must have a survey loaded in order to tabulate it.");
            return;
        }
        this.survey.tabulate();
    }

    /**
     * Switch statement to take user input from menu and run the appropriate command
     *
     * @param command user selected menu item
     * */
    public void runMainMenuCommand(int command){

        switch(command) {
            case 0: //Create
                create();
                break;
            case 1://display
                display();
                break;
            case 2://load
                load();
                break;
            case 3://save
                save();
                break;
            case 4://take
                take();
                break;
            case 5://modify
                modify();
                break;
            case 6://tabulate
                tabulate();
                break;
        }
    }

}
