import suvey.Survey;
import suvey.SurveyResponse;
import suvey.Test;
import utils.FileUtils;
import utils.Output;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

public class TestMainMenu extends MainMenu {

    private Test test;

    TestMainMenu(){

        // test menu commands
        this.commands = new ArrayList<>(Arrays.asList(
                "Create a new Test",
                "Display an existing Test without correct answers",
                "Display an existing Test with correct answers",
                "Load an existing Test",
                "Save the current Test",
                "Take the current Test",
                "Modify the current Test",
                "Tabulate a Test",
                "Grade a Test",
                "Return to the previous menu"));
    }

    /**
     * Run the creation of a new survey
     * */
    @Override
    public void create(){
        test = new Test();
        CreateMenu createMenu = new CreateMenu(test);
        test.generateName();
        createMenu.run();
    }

    /**
     * Run the process of taking a test
     * */
    @Override
    public void take() {
        if (this.test == null){
            Output.println("You must have a test loaded in order to take it.");
            return;
        }

        this.test.take();
    }


    /**
     * Run the process of modifying a test
     * */
    @Override
    public void modify() {
        if (this.test == null){
            Output.println("You must create or load a test before it can be modified!");
            return;
        }

        this.test.modifyQuestion();
    }


    /**
     * Serialize and save survey object
     * */
    @Override
    public void save() {
        if (this.test == null){
            Output.println("You must have a test loaded in order to save it.");
            return;
        }

        this.test.save();
        displayWithAnswers();
        Output.println("Test Saved\n");
    }


    /**
     * load a serialized test into memory
     * */
    @Override
    public void load() {

        String selectedPath;

        Output.println("Pick a test to load.");

        try {
            selectedPath = FileUtils.listAndPickFileFromDir(Test.SERIAL_BASE_PATH);
        } catch (IllegalStateException e) {
            Output.println("You must save a test before one can be loaded!\n");
            return;
        }

        this.test = Test.load(selectedPath);
    }


    @Override
    public void tabulate() {
        if (this.test == null){
            Output.println("You must have a test loaded in order to tabulate it.");
            return;
        }
        this.test.tabulate();
    }

    @Override
    public void runMainMenuCommand(int command) {
        switch(command) {
            case 0: //Create
                create();
                break;
            case 1://display without answers
                displayWithoutAnswers();
                break;
            case 2://display with answers
                displayWithAnswers();
                break;
            case 3://load
                load();
                break;
            case 4://save
                save();
                break;
            case 5://take
                take();
                break;
            case 6://modify
                modify();
                break;
            case 7://tabulate
                tabulate();
                break;
            case 8: //grade
                grade();
                break;
        }
    }

    private void displayWithoutAnswers() {
        test.displayWithoutAnswers();
    }

    private void displayWithAnswers() {
        test.displayWithAnswers();
    }

    /**
     * Pick survey and response and grade it
     * */
    private void grade() {

        Output.println("Select an existing test to grade.");
        String selectedPath;
        SurveyResponse response;

        try {
            selectedPath = FileUtils.listAndPickFileFromDir(Test.SERIAL_BASE_PATH);
        } catch (IllegalStateException e) {
            Output.println("You must save a test before one can be graded!\n");
            return;
        }

        this.test = Test.load(selectedPath);

        try {
            selectedPath = FileUtils.listAndPickFileFromDir(SurveyResponse.generateSerialBasePath(this.test));
        } catch (IllegalStateException e) {
            Output.println("You must take the test before a response can be graded!\n");
            return;
        }

        response = SurveyResponse.load(selectedPath);

        this.test.grade(response);
    }
}
