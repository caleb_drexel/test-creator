import suvey.*;
import utils.Input;
import utils.Output;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CreateMenu {

    private Survey survey;


    //Create menu options
    protected final List<String> createMenuCommands = new ArrayList<>(Arrays.asList(
            "Add a new T/F question",
            "Add a new multiple-choice question",
            "Add a new short answer question",
            "Add a new essay question",
            "Add a new date question",
            "Add a new matching question",
            "Return to previous menu"));

    public CreateMenu(Survey survey) {
        this.survey = survey;
    }

    /**
     * Main loop for running the create menu. Runs the menu until the user quits
     *
     * */


    public void run(){

        int command;
        Question question;
        while(true){

            command = getMenuCommand(this.createMenuCommands);

            if (command == 6){
                //quit command
                break;
            }

            question = getQuestion(command);
            question.build();
            survey.addQuestion(question);

        }
    }


    /**
     * Switch statement to take user input build the correct class
     *  this is a factory method
     *
     * @param command user selected menu item
     * */
    protected Question getQuestion(int command){

        Question question;
        switch(command) {
            case 0: //T/F question
                question = new TrueFalseQuestion();
                return question;
            case 1://multiple-choice
                question = new MultipleChoiceQuestion();
                return question;
            case 2://short answer
                question = new ShortAnswerQuestion();
                return question;
            case 3://essay question
                question = new EssayQuestion();
                return question;
            case 4://date question
                question = new DateQuestion();
                return question;
            case 5://matching
                question = new MatchingQuestion();
                return question;
            default:
                throw new IllegalStateException("Class selector reached default");
        }
    }

    //display menu and get user response
    protected int getMenuCommand(List<String> options){

        int selected;

        Output.outputOptions(options);
        selected = Input.readIntInRange(0, options.size()-1);

        return selected;

    }
}
